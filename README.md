# Eponymous 4 2015 Theme

A custom theme for [Eponymous 4](http://eponymous4.com/).

## Dependencies

* [Observant Records 2015 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2015-for-wordpress)

### Observant Records Artist Connector

The `archive-album`, `content-album` and `content-track` templates query the Observant Records artist database for discography data.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress)
for usage.
